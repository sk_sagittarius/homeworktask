﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Web;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using unirest_net.http;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace HomeworkTasks
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void Func()
        {
            var numberFacts = new NumberFacts();

            try
            {
               // var month = Convert.ToInt32(selectedMonth.Text);
                var month = selectedMonth.Text;
                var day = Convert.ToInt32(selectedDay.Text);


                if(Regex.IsMatch(month, "^[0-9]+$"))
                {
                    if(int.Parse(month)<=12)
                    {
                        if (day > 0 && day < 32)
                        {
                            Task<HttpResponse<string>> response = Unirest.get($"https://numbersapi.p.rapidapi.com/{month}/{day}/date?fragment=true&json=true")
                                .header("X-RapidAPI-Host", "numbersapi.p.rapidapi.com")
                                .header("X-RapidAPI-Key", "231b51b649mshf6a4ccf75ec064fp1eb6dejsn9181e1401766")
                                .asJsonAsync<string>();

                            var result = response.Result.Body.ToString();
                            var facts = JsonConvert.DeserializeObject<NumberFacts>(result);
                            text.Text = facts.Text;
                            year.Text = facts.Year.ToString();
                        }
                        else
                        {
                            MessageBox.Show("Wrong date");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Wrong date");
                    }
                }
                else
                {
                    MessageBox.Show("Wrong date");
                }
            }
            catch (FormatException exception)
            {
                MessageBox.Show("Wrong format");
            }

            
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            Func();
        }
    }
}
